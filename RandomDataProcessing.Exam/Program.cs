﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RandomDataProcessing.Core;
using RandomDataProcessing.Core.ICE;
using RandomDataProcessing.Extensions;
using ZedGraph;

namespace RandomDataProcessing.Exam
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {


         //  var inputValues =  FilterProcessing.GetPgp(new[] {10, 50, 100}, new[] {20, 40, 60}, 1000, 0.0001f);
            var inputValues = ValuesHelper.GetXValues(100).Select(d =>  MathHelper.Sin(d*10)).ToArray();
            Task.Run(() => new GraphForm(inputValues, "input values").ShowDialog());

            var fourierValues = FourierProcessing.GetFourierEx(inputValues);

            Task.Run(() => new GraphForm(fourierValues, "fourier values", true).ShowDialog());

            var reconstructedValues = FourierProcessing.GetReverseFourierEx(fourierValues);

            Task.Run(() => new GraphForm(reconstructedValues, "reconstructed values").ShowDialog());

            Console.ReadLine();
            //var file1 = FileProcessing.LoadDataFromFile(_file1Path);
            //var file2 = FileProcessing.LoadDataFromFile(_file2Path);

            //var task1 = Task.Factory.StartNew(() =>
            //{ var graph1Form = new GraphForm(file1,"values1");
            //    graph1Form.ShowDialog();
            //});

            //var task5 =
            //    Task.Factory.StartNew(
            //        () =>
            //        {
            //            var spectr1orm = new GraphForm(FourierProcessing.GetFourier(file1), "spectr1", true);
            //            spectr1orm.ShowDialog();
            //        });
            //var task2 = Task.Factory.StartNew(() =>
            //{
            //    var graph2Form = new GraphForm(file2,"values2");
            //    graph2Form.ShowDialog();
            //});
            //var task4 =
            //    Task.Factory.StartNew(
            //        () =>
            //        {
            //            var spectr2Form = new GraphForm(FourierProcessing.GetFourier(file2), "spectr2", true);
            //            spectr2Form.ShowDialog();
            //        });

            //var check1 = RandomQualityChecker.Check(file1);
            //var check2 = RandomQualityChecker.Check(file2);
            //var filter1 = FilterProcessing.BandPassFilter(150, 400,64 ,0.0001d);
            //var conv1 = FilterProcessing.Convolution(check2.Values, filter1);
            //var task355 = Task.Factory.StartNew(() =>
            //{
            //    var graph3Form = new GraphForm(conv1, "conv");
            //    graph3Form.ShowDialog();
            //});
            //var filter2 = FilterProcessing.LowPassFilter(150, 64, 0.0001d);
            //var conv2 = FilterProcessing.Convolution(check1.Values, filter2);
            //var task356 = Task.Factory.StartNew(() =>
            //{
            //    var graph3Form = new GraphForm(conv2, "conv");
            //    graph3Form.ShowDialog();
            //});
            //#region d
            //FirstStatistics myFS = new FirstStatistics(check1.Values.Select(x => (float) x).ToArray(), 1);
            //myFS.AV();
            //myFS.SK();
            //myFS.SKO();
            //myFS.D();
            //myFS.SO();
            //myFS.ASYM();
            //myFS.KURT();
            //myFS.C();
            //myFS.E();
            //#endregion
            //var crossCorrelation =
            //    myFS.FVK_Rxy(check2.Values.Select(x => (float) x).ToArray()).Select(x => (double) x).ToArray();
            ////  var crossCorellation = Com
            //var task3 = Task.Factory.StartNew(() =>
            //{
            //    var graph3Form = new GraphForm(crossCorrelation, "crosscorrelation");
            //    graph3Form.ShowDialog();
            //});
            //var task333 = Task.Factory.StartNew(() =>
            //{
            //    var graph3Form = new GraphForm(FourierProcessing.GetFourier(crossCorrelation), "spectr crosscorrelation",true);
            //    graph3Form.ShowDialog();
            //});
            //Task.WaitAll(task1, task2, task3);
            //Application.EnableVisualStyles();
            //Application.Run(graph1Form); // or whatever
        }
    }
    class FirstStatistics
    {
        private float[] _X;
        public float[] X { get { return _X; } set { _X = value; } }

        private int _S;
        public int S { get { return _S; } set { _S = value; } }

        private float _AV_f;
        public float AV_f { get { return _AV_f; } }

        private float _SK_f;
        public float SK_f { get { return _SK_f; } }

        private float _SKO_f;
        public float SKO_f { get { return _SKO_f; } }

        private float _D_f;
        public float D_f { get { return _D_f; } }

        private float _SO_f;
        public float SO_f { get { return _SO_f; } }

        private float _C_f;
        public float C_f { get { return _C_f; } }

        private float _E_f;
        public float E_f { get { return _E_f; } }

        private float _ASYM_f;
        public float ASYM_f { get { return _ASYM_f; } }

        private float _KURT_f;
        public float KURT_f { get { return _KURT_f; } }

        public FirstStatistics(float[] x, int S)
        {
            _X = x;
            _S = S;
        }

        public float AV()
        {
            float p = 0;
            for (int i = 0; i < _X.Length; i++)
            {
                p += _X[i];
            }
            _AV_f = (float)p / _X.Length;
            return _AV_f;
        }

        public float SK()
        {
            float p = 0;
            for (int i = 0; i < _X.Length; i++)
            {
                p += _X[i] * _X[i];
            }
            _SK_f = (float)p / _X.Length;
            return _SK_f;
        }

        public float SKO()
        {
            SK();
            _SKO_f = (float)Math.Sqrt(_SK_f);
            return (float)_SKO_f;
        }

        public float D()
        {
            float p = 0;
            for (int i = 0; i < _X.Length; i++)
            {
                p += (float)Math.Pow((double)(_X[i] - _AV_f), 2);
            }
            _D_f = (float)p / _X.Length;
            return _D_f;
        }

        public float SO()
        {
            _SO_f = (float)Math.Sqrt(_D_f);
            return _SO_f;
        }

        public float ASYM()
        {
            float p = 0;
            for (int i = 0; i < _X.Length; i++)
            {
                p += (float)Math.Pow((double)(_X[i] - _AV_f), 3);
            }
            _ASYM_f = (float)p / _X.Length;
            return _ASYM_f;
        }

        public float KURT()
        {
            float p = 0;
            for (int i = 0; i < _X.Length; i++)
            {
                p += (float)Math.Pow((double)(_X[i] - _AV_f), 4);
            }
            _KURT_f = (float)p / _X.Length;
            return _KURT_f;
        }

        public float C()
        {
            _C_f = (float)_ASYM_f / (float)Math.Pow(_SO_f, 3);
            return _C_f;
        }

        public float E()
        {
            _E_f = (float)_KURT_f / (float)Math.Pow(_SO_f, 4) - 3;
            return _E_f;
        }

        private float _KOVARIATION_Rxx(int L)
        {
            int N = _X.Length;
            float[] RES = new float[N];
            float R = 0;
            for (int i = 0; i < N - L; i++)
            {
                R += (_X[i] - _AV_f) * (_X[i + L] - _AV_f);
            }
            return (float)R / D();
        }

        public float[] KOVARIATION_Rxx()
        {
            int N = _X.Length;
            float[] RES = new float[N];
            for (int i = 0; i < N; i++)
            {
                RES[i] = _KOVARIATION_Rxx(i);
            }
            return RES;
        }

        private float _AKF_Rxx(int L)
        {
            int N = _X.Length;

            float R = 0;
            for (int i = 0; i < N - L; i++)
            {
                R += (_X[i] - _AV_f) * (_X[i + L] - _AV_f);
            }
            return (float)R / N;
        }

        public float[] AKF_Rxx()
        {
            int N = _X.Length;
            float[] RES = new float[N];
            for (int i = 0; i < N; i++)
            {
                RES[i] = _AKF_Rxx(i);
            }
            return RES;
        }

        public float[] FVK_Rxy(float[] Y)
        {
            FirstStatistics FS_Y = new FirstStatistics(Y, _S);
            float AVY = FS_Y.AV();
            float DY = FS_Y.D();
            int N = _X.Length;
            float[] RES = new float[N];
            float R;
            for (int L = 0; L < N; L++)
            {
                R = 0;
                for (int i = 0; i < N - L; i++)
                {
                    R += (_X[i] - _AV_f) * (Y[i + L] - AVY);
                }
                RES[L] = (float)R / (float)Math.Sqrt(_D_f * DY);
            }
            float max = maxArr(RES);
            for (int L = 0; L < N; L++)
            {
                RES[L] = (float)RES[L] / max;
            }
            return RES;
        }

        private float maxArr(float[] x)
        {
            float max = x[0];
            for (int i = 1; i < x.Length; i++) if (x[i] > max) max = x[i];
            return max;
        }
    }
    class GraphForm : Form
    {
        public GraphForm(double[] values, string text, bool spectr = false)
        {
            Width = 400;
            Height = 400;
            this.Text = text;
            var zedGraphControl = new ZedGraphControl {Dock = DockStyle.Fill};
            if (spectr)
                zedGraphControl.AddCurve(ValuesHelper.GetSpectrXValues(values.Length, 1000), values);
            else
                zedGraphControl.AddCurve(ValuesHelper.GetXValues(values.Length), values);
            Controls.Add(zedGraphControl);
        }
    }
}
