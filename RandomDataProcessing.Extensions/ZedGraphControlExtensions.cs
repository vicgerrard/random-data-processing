﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZedGraph;

namespace RandomDataProcessing.Extensions
{
    public static class ZedGraphControlExtensions
    {
        /// <summary>
        /// Метод расширения, позволяющий легко добавить новую кривую на график.
        /// </summary>
        /// <param name="control">Куда добавлять</param>
        /// <param name="xValues">Значения по оси абсцисс</param>
        /// <param name="yValues">Значения по оси ординат</param>
        /// <param name="clear">Надо ли очищать</param>
        public static void AddCurve(this ZedGraphControl control, double[] xValues, double[] yValues, bool clear = true)
        {
            control.Invoke(new Action(() =>
            {
                if (clear)
                    control.GraphPane.CurveList.Clear();
                var curve = control.GraphPane.AddCurve("", xValues, yValues, Color.Red);
                curve.Line.IsVisible = true;
                curve.Symbol.Size = .1f;
                control.AxisChange();
                control.Invalidate();
            }));
        }
    }
}
