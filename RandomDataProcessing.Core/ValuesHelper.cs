﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomDataProcessing.Core
{
    public class ValuesHelper
    {
        /// <summary>
        /// Возвращает массив последовательных значений от 0 до count
        /// </summary>
        /// <param name="count">Количество значений</param>
        public static double[] GetXValues(int count)
        {
            return Enumerable.Range(0, count).Select(x => (double) x).ToArray();
        }

        public static double[] GetSpectrXValues(int count, double rate)
        {
            return GetXValues(count).Select(x => x*(rate/count)).ToArray();
        }
    }
}
