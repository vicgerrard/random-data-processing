﻿using System;
using System.Linq;

namespace RandomDataProcessing.Core
{
    public static class FourierProcessing
    {

        public static double[] GetFourierEx(double[] array)
        {
            var result = new double[array.Length];
            Parallel.For(0, result.Length, i =>
            {
                var im = GetIm(array, i);
                var re = GetRe(array, i);
                result[i] = im + re;
            });
            return result.Select(x => x * result.Length).ToArray();
        }

        public static double[] GetReverseFourierEx(double[] array)
        {
            var result = new double[array.Length];
            Parallel.For(0, result.Length, i =>
            {
                var im = GetReverseIm(array, i);
                var re = GetReverseRe(array, i);
                result[i] = im + re;
            });
            return result;
        }

        public static double[] GetFourier(double[] array)
        {
            var result = new double[array.Length/2];
            Parallel.For(0, result.Length, i =>
            {
                var im = GetIm(array, i);
                var re = GetRe(array, i);
                result[i] = Math.Sqrt(im*im + re*re);
            });
            return result.Select(x => x*result.Length).ToArray();
        }


        public static double[] GetPhaseFourier(double[] array)
        {
            var result = new double[array.Length/2];
            Parallel.For(0, result.Length, i =>
            {
                var im = GetIm(array, i);
                var re = GetRe(array, i);
                result[i] = Math.Atan2(im, re)*180/Math.PI;
            });
            return result.ToArray();
        }

        private static double GetRe(double[] array, int k)
        {
            double result = 0;
            for (var i = 0; i < array.Length; i++)
            {
                result += array[i]*Math.Cos(2*Math.PI*k*i/array.Length);
            }
            return result/array.Length;
        }

        private static double GetIm(double[] array, int k)
        {
            double result = 0;
            for (var i = 0; i < array.Length; i++)
            {
                result += array[i]*Math.Sin(2*Math.PI*k*i/array.Length);
            }
            return result/array.Length;
        }
    }
}
