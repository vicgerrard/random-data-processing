﻿using System;
using System.Linq;

namespace RandomDataProcessing.Core
{
    public static class RandomQualityChecker
    {
        public static QualityResult Check(double[] values)
        {
            double mean = GetMean(values);
            double meanSq = GetMeanSq(values);
            double rootMeanS = GetRootMeanS(meanSq);
            double dev = GetDev(values, mean);
            double standDev = GetStandDev(dev);
            double asym = GetAsym(values, mean, standDev);
            double curtos = GetCurtos(values, mean, standDev);
            double[] covariation = GetCovariation(values, mean, dev);
            double[] autocovariation = GetAutoCovariation(values, mean);

            var distrubutionValues = GetDistributionValues(values);
            return new QualityResult(mean, meanSq, rootMeanS, dev, standDev, asym, curtos, values, covariation, autocovariation, distrubutionValues);
        }

        private static DistributionValues GetDistributionValues(double[] values)
        {
            values = values.OrderBy(x => x).ToArray();
            double range = values.Max() - values.Min();
            double k = Math.Ceiling(Math.Log(values.Length, 2) + 1);
            var intervalSize = range / k;
            var frequences = new int[(int)k];
            for (var i = 0; i < frequences.Length; i++)
            {
                frequences[i] = 0;
                var min = values[0] + intervalSize * i;
                var max = min + intervalSize;
                for (var j = 0; j < values.Length; j++)
                    if (values[j] >= min && values[j] < max) frequences[i]++;
            }

            var ws = new double[(int)k];
            for (var i = 0; i < ws.Length; i++)
            {
                ws[i] = (double)frequences[i] / values.Length;
            }

            var emps = new double[(int)k];
            for (var i = 0; i < emps.Length; i++)
            {
                emps[i] = ws[i] / intervalSize;
            }

            var wDivSizes = new double[(int)k];
            for (var i = 0; i < wDivSizes.Length; i++)
            {
                wDivSizes[i] = ws[i] / intervalSize;
            }
            return new DistributionValues(Enumerable.Range(0, (int) k + 1).Select(x => values[0] + intervalSize*x).ToArray(), emps);
        }

        public static double[] GetAutoCovariation(double[] first, double[] second)
        {
            double[] result = new double[first.Length];
            for (var i = 0; i < result.Length; i++)
            {
                var curResult = 0d;
                for (int j = 0; j < result.Length - i; j++)
                {
                    curResult += (first[j] - first.Average()) * (second[j + i] - second.Average());
                }
                result[i] = curResult/Math.Sqrt(GetDev(first, first.Average())*GetDev(second, second.Average()));
            }
            return result;
        }
        public static double[] GetAutoCovariation(QualityResult first, QualityResult second)
        {

            double[] result = new double[first.Values.Length];
            for (var i = 0; i < result.Length; i++)
            {
                var curResult = 0d;
                for (int j = 0; j < result.Length - i; j++)
                {
                    curResult += (first.Values[j] - first.Mean)*(second.Values[j + i] - second.Mean);
                }
                result[i] = curResult/Math.Sqrt(first.Dev*second.Dev);
            }
            return result;
        }
        private static double[] GetAutoCovariation(double[] values, double mean)
        {
            var result = new double[values.Length];
            for (int i = 0; i < values.Length; i++)
            {
                var curResult = 0d;
                for (int j = 0; j < values.Length - i; j++)
                {
                    curResult += (values[j] - mean)*(values[j + i] - mean);
                }
                result[i] = curResult / values.Length;
            }
            return result;
        }

        private static double[] GetCovariation(double[] values, double mean, double dev)
        {
            var result = new double[values.Length];
            for (int i = 0; i < values.Length; i++)
            {
                var curResult = 0d;
                for (int j = 0; j < values.Length - i; j++)
                {
                    curResult += (values[j] - mean)*(values[j + i] - mean);
                }
                result[i] = curResult / dev;
            }
            return result;
        }

        private static double[] FormatTo01(double[] values)
        {
            double[] result = new double[values.Length];
            double min = values.Min();
            double max = values.Max();

            for (var i = 0; i < values.Length; i++)
            {
                result[i] = ((values[i] - min)/(max - min) - 1d/2)*2;
            }
            return result;
        }

        private static double GetCurtos(double[] values, double mean, double standDev)
        {
            double sum = 0;
            for (var i = 0; i < values.Length; i++)
            {
                sum += (values[i] - mean) * (values[i] - mean) * (values[i] - mean) * (values[i] - mean);
            }
            var m3 = 1d / values.Length * sum;
            return m3/(standDev*standDev*standDev*standDev) - 3;
        }

        private static double GetAsym(double[] values, double mean, double standDev)
        {
            double sum = 0;
            for (var i = 0; i < values.Length; i++)
            {
                sum += (values[i] - mean)*(values[i] - mean)*(values[i] - mean);
            }
            var m3 = 1d/values.Length*sum;
            return m3/(standDev*standDev*standDev);
        }

        private static double GetStandDev(double dev)
        {
            return Math.Sqrt(dev);
        }

        private static double GetDev(double[] values, double mean)
        {
            double sum = 0;
            for (var i = 0; i < values.Length; i++)
            {
                sum += (values[i] - mean)*(values[i] - mean);
            }
            return 1d/values.Length*sum;
        }

        private static double GetRootMeanS(double mean)
        {
            return Math.Sqrt(mean);
        }

        private static double GetMeanSq(double[] values)
        {
            double sum = 0;
            for (var i = 0; i < values.Length; i++)
            {
                sum += values[i]*values[i];
            }
            return 1d/values.Length*sum;
        }

        private static double GetMean(double[] values)
        {
            double sum = 0;
            for (int i = 0; i < values.Length; i++)
            {
                sum += values[i];
            }
            return 1d/values.Length*sum;
        }
    }

    public struct QualityResult
    {
        public double Mean;
        public double MeanSq;
        public double RootMeanS;
        public double Dev;
        public double StandDev;
        public double Asym;
        public double Curtos;
        public double[] Values;
        public double[] Covariation;
        public double[] AutoCovariation;
        public DistributionValues DistributionValues;
        public QualityResult(double mean, double meanSq, double rootMeanS, double dev, double standDev, double asym, double curtos, double[] values, double[] covariation, double[] autoCovariation, DistributionValues distributionValues)
        {
            Mean = mean;
            MeanSq = meanSq;
            RootMeanS = rootMeanS;
            Dev = dev;
            StandDev = standDev;
            Asym = asym;
            Curtos = curtos;
            Values = values;
            Covariation = covariation;
            AutoCovariation = autoCovariation;
            DistributionValues = distributionValues;
        }

        public override string ToString()
        {
            return $"{nameof(Mean)}: {Mean}\r\n {nameof(MeanSq)}: {MeanSq}\r\n {nameof(RootMeanS)}: {RootMeanS}\r\n {nameof(Dev)}: {Dev}\r\n {nameof(StandDev)}: {StandDev}\r\n {nameof(Asym)}: {Asym}\r\n {nameof(Curtos)}: {Curtos}";
        }
    }

    public struct DistributionValues
    {
        public double[] XValues;
        public double[] YValues;

        public DistributionValues(double[] xValues, double[] yValues)
        {
            XValues = xValues;
            YValues = yValues;
        }
    }
}
