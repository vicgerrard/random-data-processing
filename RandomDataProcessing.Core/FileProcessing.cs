﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomDataProcessing.Core
{
    public class FileProcessing
    {
        public static double[] LoadDataFromFile(string path)
        {
            var result = new List<double>();
            var bytes = File.ReadAllBytes(path);
            for (int i = 0; i < bytes.Length; i += 4)
                result.Add(BitConverter.ToSingle(bytes, i));
            return result.ToArray();
        }
    }
}
