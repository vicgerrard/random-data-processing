﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;
using NAudio.Wave;

namespace RandomDataProcessing.Core
{
    public class WavProcessing
    {
        private static WaveFormat _currentFormat;
        public static double[] ReadWav(string path, out WaveFormat format)
        {
            using (var reader = new WaveFileReader(path))
            {
                _currentFormat = reader.WaveFormat;
                WaveChannel32 wave = new WaveChannel32(reader);
                int sampleSize = reader.WaveFormat.SampleRate;
                var bufferSize = sampleSize;
                var buffer = new byte[bufferSize];
                List<double> yValues = new List<double>();
                while (wave.Position < wave.Length)
                {
                    var read = wave.Read(buffer, 0, bufferSize);
                    for (int i = 0; i < read; i+=4)
                    {
                        if (i + 4 >= read) break;
                        var point = BitConverter.ToSingle(buffer, i);
                        yValues.Add(point);
                    }
                }
                format = _currentFormat;
                return yValues.ToArray();
            }
        }
        
        public static void WriteWav(string path, double[] bytes)
        {
            var floats = bytes.Select(x => (float) x).ToArray();
            using (var waveWriter = new WaveFileWriter(path, _currentFormat))
                waveWriter.WriteSamples(floats, 0, floats.Length);
        }

        public static double[] ConvertWavToDoubles(Stream wavStream)
        {
            var waveReader = new WaveFileReader(wavStream);
            WaveChannel32 wave = new WaveChannel32(waveReader);
            int sampleSize = 1024;
            var bufferSize = 16384 * sampleSize;
            var buffer = new byte[bufferSize];
            List<double> yValues = new List<double>();
            while (wave.Position < wave.Length)
            {
                var read = wave.Read(buffer, 0, bufferSize);
                for (int i = 0; i < read / sampleSize; i++)
                {
                    var point = BitConverter.ToSingle(buffer, i * sampleSize);
                    yValues.Add(point);
                }
            }
            waveReader.Dispose();
            return yValues.ToArray();
        }

        public static float[] ConvertWavToFloats(string voiceFile)
        {
            var waveReader = new WaveFileReader(voiceFile);
            WaveChannel32 wave = new WaveChannel32(waveReader); int sampleSize = 1024;
            var bufferSize = 16384 * sampleSize;
            var buffer = new byte[bufferSize];
            List<float> yValues = new List<float>();
            while (wave.Position < wave.Length)
            {
                var read = wave.Read(buffer, 0, bufferSize);
                for (int i = 0; i < read / sampleSize; i++)
                {
                    var point = BitConverter.ToSingle(buffer, i * sampleSize);
                    yValues.Add(point);
                }
            }
            waveReader.Dispose();
            return yValues.ToArray();
        }
    }
}
