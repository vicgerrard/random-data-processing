﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomDataProcessing.Core
{
    public class FilterProcessing
    {
        public static double[] GetPgp(int[] a, int[] f, int n, float dt)
        {
            float[][] T = new float[a.Length][];
            for (int i = 0; i < a.Length; i++)
            {
                T[i] = first_Xk(a[i], f[i], n, dt);
            }
            double[] RES = new double[n];
            for (int i = 0; i < n; i++)
            {
                float temp = 0;
                for (int j = 0; j < a.Length; j++)
                {
                    temp += T[j][i];
                }
                RES[i] = temp;
            }
            return RES;
        }
        private static float _first_Xk(float A0, float f0, float dt, int k)
        {
            return A0 * (float)Math.Sin((double)2 * (float)Math.PI * f0 * dt * k);
        }   
        private static float[] first_Xk(float A0, float f0, int _N, float _dt)
        {
            float[] res = new float[_N];
            for (int k = 0; k < _N; k++)
            {
                res[k] = _first_Xk(A0, f0, _dt, k);
            }
            return res;
        }
        public static double[] Convolution(double[] yValues, double[] dValues)
        {
            double[] zValues = new double[yValues.Length + dValues.Length - 1];
            for (var k = 0; k < yValues.Length + dValues.Length - 1; k++)
            {
                double summ = 0;
                for (var i = 0; i < dValues.Length - 1; i++)
                {
                    if (k - i < yValues.Length & i < k)
                    { summ += yValues[k - i] * dValues[i]; }
                }
                zValues[k] = summ;
            }
            zValues = zValues.Skip(31).Reverse().Skip(31).Reverse().ToArray();
            return zValues;
        }
        public static double[] LowPassFilter(double fcut, int m, double dt)
        {
            double[] constants =
            {
                0.35577019,
                0.2436983,
                0.07211497,
                0.00630165
            };
            double arg = fcut*dt;
            double[] lpw = new double[m + 1];
            lpw[0] = arg;
            arg *= Math.PI;
            for (int i = 1; i <= m; i++)
                lpw[i] = Math.Sin(arg*i)/(Math.PI*i);
            lpw[m] /= 2;

            var sumg = lpw[0];
            for (int i = 1; i <= m; i++)
            {
                var sum = constants[0];
                arg = Math.PI*i/m;
                for (int k = 1; k < constants.Length; k++)
                    sum += 2*constants[k]*Math.Cos(arg*k);
                lpw[i] *= sum;
                sumg += 2*lpw[i];
            }
            for (int i = 0; i <= m; i++)
                lpw[i] /= sumg;

            var result = new double[m*2 + 1];

            for (int i = 0; i < m+1; i++)
            {
                result[m+i] = lpw[i];
            }
            for (int i = 0; i < m; i++)
            {
                result[i] = lpw[lpw.Length - i - 1];
            }
            return result;
        }

        public static double[] HighPassFilter(double fcut, int m, double dt)
        {
            var lpw = LowPassFilter(fcut, m, dt);
            var hpw = new double[2*m + 1];
            for (int k = 0; k < 2*m + 1; k++)
            {
                if (k == m)
                    hpw[k] = 1 - lpw[k];
                else hpw[k] = -lpw[k];
            }
            return hpw;
        }

        public static double[] BandPassFilter(double fcut1, double fcut2, int m, double dt)
        {
            var lpw1 = LowPassFilter(fcut1, m, dt);
            var lpw2 = LowPassFilter(fcut2, m, dt);
            var bpf = new double[2*m+1];
            for (int k = 0; k < 2*m+1; k++)
                bpf[k] = lpw2[k] - lpw1[k];
            return bpf;
        }

        public static double[] BandStopFilter(double fcut1, double fcut2, int m, double dt)
        {
            var lpw1 = LowPassFilter(fcut1, m, dt);
            var lpw2 = LowPassFilter(fcut2, m, dt);
            var bsf = new double[2*m+1];
            for (int k = 0; k < 2*m+1; k++)
            {
                if (k == m)
                    bsf[k] = 1 + lpw1[k] - lpw2[k];
                else
                    bsf[k] = lpw1[k] - lpw2[k];
            }
            return bsf;
        }
    }
}
