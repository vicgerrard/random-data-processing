﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NAudio.Wave;

namespace RandomDataProcessing.Core.ICE
{
    public class Engine
    {
        public class WorkResult
        {
            public List<double> Speed;
            public List<double> Acceleration;
            public WorkResult()
            {
                Speed = new List<double>();
                Acceleration = new List<double>();
            }
        }

        private readonly Dictionary<int, int> _tactToWorkCylinder;
        public const int CyclesCount = 6;
        public const int TactLength = 180;
        public double CurrentSpeed { get; set; } = 850;
        public double CurrentAcceleration { get; set; }
        private readonly double[] _cylinders;
        private WorkResult _workResult;
        public Engine()
        {
            _cylinders = new double[4];
            _tactToWorkCylinder = new Dictionary<int, int>
            {
                [0] = 0,
                [1] = 2,
                [2] = 3,
                [3] = 1
            };
        }

        public WorkResult Work()
        {
            _workResult = new WorkResult();
            for (int i = 0; i < CyclesCount; i++)
                ProcessCycle();
            return _workResult;
        }

        private void ProcessCycle()
        {
            for (int i = 0; i < 4; i++)
                ProcessTact(i);
        }

        private void ProcessTact(int tact)
        {
            var powerCylinder = _tactToWorkCylinder[tact%4];
            for (int i = TactLength/2; i > -TactLength/2; i--)
            {
                if (powerCylinder == 2 )
                {
                    CurrentSpeed -= MathHelper.Sin(i)/2d;
                    //  CurrentSpeed += MathHelper.Sin(i*2d)/4d;
                }
                else
                {
                    CurrentSpeed += MathHelper.Sin(i*2d);
                }
             
                UpdateSpeed();
            }
        }

        private void UpdateSpeed()
        {
            //CurrentSpeed += MathHelper.Sin(CurrentAcceleration);
            _workResult.Speed.Add(CurrentSpeed);
            _workResult.Acceleration.Add(CurrentAcceleration);
        }

        //private void UpdateSpeed(int workCylinder, bool invert = false)
        //{
        //    for (var i = 0; i < _cylindersAcceleration.Length; i++)
        //    {
        //        if (i == workCylinder)
        //            _cylindersAcceleration[i] = 1;
        //        else
        //            _cylindersAcceleration[i] = 0;
        //    }
        //    for (int i = 0; i < TactLength; i++)
        //    {
        //        var cylinder1 = _cylindersAcceleration[0];
        //        var cylinder2 = _cylindersAcceleration[1];
        //        var cylinder3 = _cylindersAcceleration[2];
        //        var cylinder4 = _cylindersAcceleration[3];
        //        var acceleration = _cylindersAcceleration.Sum();
        //        if (invert) acceleration = -acceleration;
        //        CurrentSpeed += MathHelper.Sin(i*acceleration*180/TactLength);
        //        _workResult.Speed.Add(CurrentSpeed);
        //        _workResult.Cylinder1.Add(cylinder1);
        //        _workResult.Cylinder2.Add(cylinder2);
        //        _workResult.Cylinder3.Add(cylinder3);
        //        _workResult.Cylinder4.Add(cylinder4);
        //        //for (var j = 0; j < _cylindersAcceleration.Length; j++)
        //        //{
        //        //    if (j == workCylinder)
        //        //        if (badCilinder)
        //        //            _cylindersAcceleration[j] += 1d / TactLength;
        //        //        else
        //        //            _cylindersAcceleration[j] -= 1d / TactLength;
        //        //    //else
        //        //    //    _cylindersAcceleration[j] += 1d/TactLength;
        //        //}
        //    }
        //}
    }

    public static class MathHelper
    {
        public static double Sin(double angle)
        {
            return Math.Sin(angle * 180d / Math.PI);
        }

        public static double Cos(double angle)
        {
            return Math.Cos(angle*Math.PI/180d);
        }
    }
}
