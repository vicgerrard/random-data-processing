﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomDataProcessing.Core
{
    public static class ICEProcessing
    {

        private static readonly Random Random = new Random();
        public static double[] GetCylinderValues(int cylinder, int n = 10000, bool bad = false)
        {
            // + Random.NextDouble()*10;
            var result = new double[n];
            for (int i = 0; i < n; i++)
            {
                if ((i + 1)%cylinder*400 == 0)
                {
                    for (;; i++)
                    {
                        result[i] = 1;
                    }
                }
                //if (value == 0)
                //{
                //    for (;; i++)
                //    {
                //        result[i] = 1;
                //    }
                //}
                //else if (i+1)
                //{
                    
                //}
                //Math.Sin(i*Math.PI/180d);
                //   result[i] = Math.Sin(((bad ? 1 : 1)*i + cylinder*90)*Math.PI/180d);
                //  result[i] = Math.Sin(((bad ? 0 : 1)*i + cylinder*Math.PI/2)*Math.PI/180);
            }
            return result;
        }

        public static double[] GetEngineValues(double[] cylinder1Values, double[] cylinder2Values, double[] cylinder3Values, double[] cylinder4Values)
        {
            double[] result = new double[cylinder1Values.Length];
            for (int i = 0; i < result.Length; i++)
                result[i] = cylinder1Values[i] + cylinder2Values[i] + cylinder3Values[i] + cylinder4Values[i];
            return result;
        }
    }
}
