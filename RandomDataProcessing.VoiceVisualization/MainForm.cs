﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RandomDataProcessing.Core;
using RandomDataProcessing.Extensions;

namespace RandomDataProcessing.VoiceVisualization
{
    public partial class MainForm : Form
    {
        private string _voiceFile = "C:\\Users\\Viktor\\Source\\Repos\\random-data-processing\\RandomDataProcessing.WavVisualization\\Resources\\Sample1.wav";
        public MainForm()
        {
            InitializeComponent();
            PrintAll();
        }

        public async void PrintAll()
        {
            await Task.Factory.StartNew(() =>
            {
                var voiceYValues = WavProcessing.ConvertWavToFloats(_voiceFile).Select(x => (double) x).ToArray();
                var voiceXValues = ValuesHelper.GetXValues(voiceYValues.Length);
                voiceGraph.AddCurve(voiceXValues, voiceYValues);

                var spectrYValues = FourierProcessing.GetFourierParallel(voiceYValues);
                var spectrXValues = ValuesHelper.GetXValues(spectrYValues.Length);
                spectrGraph.AddCurve(spectrXValues, spectrYValues);
            });
        }
    }
}
