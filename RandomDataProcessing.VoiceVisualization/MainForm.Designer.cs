﻿namespace RandomDataProcessing.VoiceVisualization
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.voiceGraph = new ZedGraph.ZedGraphControl();
            this.spectrGraph = new ZedGraph.ZedGraphControl();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.spectrGraph, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.voiceGraph, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(662, 329);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // voiceGraph
            // 
            this.voiceGraph.Dock = System.Windows.Forms.DockStyle.Fill;
            this.voiceGraph.Location = new System.Drawing.Point(3, 3);
            this.voiceGraph.Name = "voiceGraph";
            this.voiceGraph.ScrollGrace = 0D;
            this.voiceGraph.ScrollMaxX = 0D;
            this.voiceGraph.ScrollMaxY = 0D;
            this.voiceGraph.ScrollMaxY2 = 0D;
            this.voiceGraph.ScrollMinX = 0D;
            this.voiceGraph.ScrollMinY = 0D;
            this.voiceGraph.ScrollMinY2 = 0D;
            this.voiceGraph.Size = new System.Drawing.Size(656, 158);
            this.voiceGraph.TabIndex = 0;
            this.voiceGraph.UseExtendedPrintDialog = true;
            // 
            // spectrGraph
            // 
            this.spectrGraph.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spectrGraph.Location = new System.Drawing.Point(3, 167);
            this.spectrGraph.Name = "spectrGraph";
            this.spectrGraph.ScrollGrace = 0D;
            this.spectrGraph.ScrollMaxX = 0D;
            this.spectrGraph.ScrollMaxY = 0D;
            this.spectrGraph.ScrollMaxY2 = 0D;
            this.spectrGraph.ScrollMinX = 0D;
            this.spectrGraph.ScrollMinY = 0D;
            this.spectrGraph.ScrollMinY2 = 0D;
            this.spectrGraph.Size = new System.Drawing.Size(656, 159);
            this.spectrGraph.TabIndex = 1;
            this.spectrGraph.UseExtendedPrintDialog = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(662, 329);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "MainForm";
            this.Text = "VoiceVizualiation";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private ZedGraph.ZedGraphControl spectrGraph;
        private ZedGraph.ZedGraphControl voiceGraph;
    }
}

