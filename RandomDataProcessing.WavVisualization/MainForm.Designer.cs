﻿namespace RandomDataProcessing.WavVisualization
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.inputGraph = new ZedGraph.ZedGraphControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.filteredSpectr = new ZedGraph.ZedGraphControl();
            this.inputSpectr = new ZedGraph.ZedGraphControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnPlayInput = new System.Windows.Forms.Button();
            this.btnPlayFiltered = new System.Windows.Forms.Button();
            this.rbBSF = new System.Windows.Forms.RadioButton();
            this.rbBPF = new System.Windows.Forms.RadioButton();
            this.rbHPF = new System.Windows.Forms.RadioButton();
            this.rbLPF = new System.Windows.Forms.RadioButton();
            this.btnOpenFile = new System.Windows.Forms.Button();
            this.filteredGraph = new ZedGraph.ZedGraphControl();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // inputGraph
            // 
            this.inputGraph.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inputGraph.Location = new System.Drawing.Point(3, 73);
            this.inputGraph.Name = "inputGraph";
            this.inputGraph.ScrollGrace = 0D;
            this.inputGraph.ScrollMaxX = 0D;
            this.inputGraph.ScrollMaxY = 0D;
            this.inputGraph.ScrollMaxY2 = 0D;
            this.inputGraph.ScrollMinX = 0D;
            this.inputGraph.ScrollMinY = 0D;
            this.inputGraph.ScrollMinY2 = 0D;
            this.inputGraph.Size = new System.Drawing.Size(382, 135);
            this.inputGraph.TabIndex = 2;
            this.inputGraph.UseExtendedPrintDialog = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.filteredSpectr, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.inputSpectr, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.inputGraph, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.filteredGraph, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(776, 352);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // filteredSpectr
            // 
            this.filteredSpectr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.filteredSpectr.Location = new System.Drawing.Point(391, 214);
            this.filteredSpectr.Name = "filteredSpectr";
            this.filteredSpectr.ScrollGrace = 0D;
            this.filteredSpectr.ScrollMaxX = 0D;
            this.filteredSpectr.ScrollMaxY = 0D;
            this.filteredSpectr.ScrollMaxY2 = 0D;
            this.filteredSpectr.ScrollMinX = 0D;
            this.filteredSpectr.ScrollMinY = 0D;
            this.filteredSpectr.ScrollMinY2 = 0D;
            this.filteredSpectr.Size = new System.Drawing.Size(382, 135);
            this.filteredSpectr.TabIndex = 7;
            this.filteredSpectr.UseExtendedPrintDialog = true;
            // 
            // inputSpectr
            // 
            this.inputSpectr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inputSpectr.Location = new System.Drawing.Point(3, 214);
            this.inputSpectr.Name = "inputSpectr";
            this.inputSpectr.ScrollGrace = 0D;
            this.inputSpectr.ScrollMaxX = 0D;
            this.inputSpectr.ScrollMaxY = 0D;
            this.inputSpectr.ScrollMaxY2 = 0D;
            this.inputSpectr.ScrollMinX = 0D;
            this.inputSpectr.ScrollMinY = 0D;
            this.inputSpectr.ScrollMinY2 = 0D;
            this.inputSpectr.Size = new System.Drawing.Size(382, 135);
            this.inputSpectr.TabIndex = 6;
            this.inputSpectr.UseExtendedPrintDialog = true;
            // 
            // panel1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel1, 2);
            this.panel1.Controls.Add(this.btnPlayInput);
            this.panel1.Controls.Add(this.btnPlayFiltered);
            this.panel1.Controls.Add(this.rbBSF);
            this.panel1.Controls.Add(this.rbBPF);
            this.panel1.Controls.Add(this.rbHPF);
            this.panel1.Controls.Add(this.rbLPF);
            this.panel1.Controls.Add(this.btnOpenFile);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(770, 64);
            this.panel1.TabIndex = 4;
            // 
            // btnPlayInput
            // 
            this.btnPlayInput.Location = new System.Drawing.Point(2, 31);
            this.btnPlayInput.Name = "btnPlayInput";
            this.btnPlayInput.Size = new System.Drawing.Size(150, 23);
            this.btnPlayInput.TabIndex = 9;
            this.btnPlayInput.Text = "Воспроизвести";
            this.btnPlayInput.UseVisualStyleBackColor = true;
            this.btnPlayInput.Click += new System.EventHandler(this.btnPlayInput_Click);
            // 
            // btnPlayFiltered
            // 
            this.btnPlayFiltered.Location = new System.Drawing.Point(388, 31);
            this.btnPlayFiltered.Name = "btnPlayFiltered";
            this.btnPlayFiltered.Size = new System.Drawing.Size(150, 23);
            this.btnPlayFiltered.TabIndex = 8;
            this.btnPlayFiltered.Text = "Воспроизвести";
            this.btnPlayFiltered.UseVisualStyleBackColor = true;
            this.btnPlayFiltered.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // rbBSF
            // 
            this.rbBSF.AutoSize = true;
            this.rbBSF.Location = new System.Drawing.Point(254, 34);
            this.rbBSF.Name = "rbBSF";
            this.rbBSF.Size = new System.Drawing.Size(94, 17);
            this.rbBSF.TabIndex = 7;
            this.rbBSF.Text = "BandStopFilter";
            this.rbBSF.UseVisualStyleBackColor = true;
            this.rbBSF.CheckedChanged += new System.EventHandler(this.rbLPF_CheckedChanged);
            // 
            // rbBPF
            // 
            this.rbBPF.AutoSize = true;
            this.rbBPF.Location = new System.Drawing.Point(254, 5);
            this.rbBPF.Name = "rbBPF";
            this.rbBPF.Size = new System.Drawing.Size(95, 17);
            this.rbBPF.TabIndex = 6;
            this.rbBPF.Text = "BandPassFilter";
            this.rbBPF.UseVisualStyleBackColor = true;
            this.rbBPF.CheckedChanged += new System.EventHandler(this.rbLPF_CheckedChanged);
            // 
            // rbHPF
            // 
            this.rbHPF.AutoSize = true;
            this.rbHPF.Location = new System.Drawing.Point(158, 34);
            this.rbHPF.Name = "rbHPF";
            this.rbHPF.Size = new System.Drawing.Size(92, 17);
            this.rbHPF.TabIndex = 5;
            this.rbHPF.Text = "HighPassFilter";
            this.rbHPF.UseVisualStyleBackColor = true;
            this.rbHPF.CheckedChanged += new System.EventHandler(this.rbLPF_CheckedChanged);
            // 
            // rbLPF
            // 
            this.rbLPF.AutoSize = true;
            this.rbLPF.Checked = true;
            this.rbLPF.Location = new System.Drawing.Point(158, 5);
            this.rbLPF.Name = "rbLPF";
            this.rbLPF.Size = new System.Drawing.Size(90, 17);
            this.rbLPF.TabIndex = 4;
            this.rbLPF.TabStop = true;
            this.rbLPF.Text = "LowPassFilter";
            this.rbLPF.UseVisualStyleBackColor = true;
            this.rbLPF.CheckedChanged += new System.EventHandler(this.rbLPF_CheckedChanged);
            // 
            // btnOpenFile
            // 
            this.btnOpenFile.Location = new System.Drawing.Point(2, 2);
            this.btnOpenFile.Name = "btnOpenFile";
            this.btnOpenFile.Size = new System.Drawing.Size(150, 23);
            this.btnOpenFile.TabIndex = 3;
            this.btnOpenFile.Text = "Выбрать файл";
            this.btnOpenFile.UseVisualStyleBackColor = true;
            this.btnOpenFile.Click += new System.EventHandler(this.btnOpenFile_Click);
            // 
            // filteredGraph
            // 
            this.filteredGraph.Dock = System.Windows.Forms.DockStyle.Fill;
            this.filteredGraph.Location = new System.Drawing.Point(391, 73);
            this.filteredGraph.Name = "filteredGraph";
            this.filteredGraph.ScrollGrace = 0D;
            this.filteredGraph.ScrollMaxX = 0D;
            this.filteredGraph.ScrollMaxY = 0D;
            this.filteredGraph.ScrollMaxY2 = 0D;
            this.filteredGraph.ScrollMinX = 0D;
            this.filteredGraph.ScrollMinY = 0D;
            this.filteredGraph.ScrollMinY2 = 0D;
            this.filteredGraph.Size = new System.Drawing.Size(382, 135);
            this.filteredGraph.TabIndex = 5;
            this.filteredGraph.UseExtendedPrintDialog = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(776, 352);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "MainForm";
            this.Text = "WavVisualization";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private ZedGraph.ZedGraphControl inputGraph;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private ZedGraph.ZedGraphControl filteredSpectr;
        private ZedGraph.ZedGraphControl inputSpectr;
        private ZedGraph.ZedGraphControl filteredGraph;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnPlayFiltered;
        private System.Windows.Forms.RadioButton rbBSF;
        private System.Windows.Forms.RadioButton rbBPF;
        private System.Windows.Forms.RadioButton rbHPF;
        private System.Windows.Forms.RadioButton rbLPF;
        private System.Windows.Forms.Button btnOpenFile;
        private System.Windows.Forms.Button btnPlayInput;
    }
}

