﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NAudio.Wave;
using RandomDataProcessing.Core;
using RandomDataProcessing.Extensions;

namespace RandomDataProcessing.WavVisualization
{
    public partial class MainForm : Form
    {
        readonly OpenFileDialog _openFileDialog = new OpenFileDialog();
        private string _fileName;
        private readonly WaveOut _waveOut = new WaveOut();
        private double[] _inputValues;
        private double[] _filteredValues;
        private WaveFileReader _reader;

        public MainForm()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (_filteredValues!=null)
            {
                _reader?.Dispose();
                _reader = new WaveFileReader("output.wav");
                _waveOut.Init(_reader);
                _waveOut.Play();
            }
        }

        private async void btnOpenFile_Click(object sender, EventArgs e)
        {
            if (_openFileDialog.ShowDialog() == DialogResult.OK)
            {
                _fileName = _openFileDialog.FileName;
                this.Enabled = false;
                ProcessAll();
                this.Enabled = true;
            }
        }

        private void ProcessAll()
        {
            _reader?.Dispose();
            WaveFormat format;
            _inputValues = WavProcessing.ReadWav(_fileName, out format).Skip(50000).Take(23000).ToArray();
            WavProcessing.WriteWav("input.wav", _inputValues);
            inputGraph.AddCurve(ValuesHelper.GetXValues(_inputValues.Length), _inputValues);
            var inputSpectr = FourierProcessing.GetFourier(_inputValues);
            var xValues = ValuesHelper.GetSpectrXValues(inputSpectr.Length, format.SampleRate);
            this.inputSpectr.AddCurve(xValues, inputSpectr);
            double fcut = 1500d;
            int m = 1000;
            double dt = 1d/format.SampleRate;
            double[] filterValues;
            double fcut1 = 300d;
            double fcut2 = 900d;
            if (rbLPF.Checked) filterValues = FilterProcessing.LowPassFilter(fcut, m, dt);
            else if (rbBPF.Checked) filterValues = FilterProcessing.BandPassFilter(fcut1, fcut2, m, dt);
            else if (rbBSF.Checked) filterValues = FilterProcessing.BandStopFilter(fcut1, fcut2, m, dt);
            else filterValues = FilterProcessing.HighPassFilter(fcut, m, dt);
            _filteredValues = FilterProcessing.Convolution(_inputValues, filterValues);
            filteredGraph.AddCurve(ValuesHelper.GetXValues(_filteredValues.Length), _filteredValues);
            WavProcessing.WriteWav("output.wav", _filteredValues);
            var filteredSpectr = FourierProcessing.GetFourier(_filteredValues);
            var filteredXValues = ValuesHelper.GetSpectrXValues(filteredSpectr.Length, format.SampleRate);
            this.filteredSpectr.AddCurve(filteredXValues, filteredSpectr);
        }

        private void btnPlayInput_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(_fileName))
            {
                _reader?.Dispose();
                _reader = new WaveFileReader("input.wav");
                _waveOut.Init(_reader);
                _waveOut.Play();
            }
        }

        private void rbLPF_CheckedChanged(object sender, EventArgs e)
        {
            if (!(sender as RadioButton).Checked) return;
            if (string.IsNullOrEmpty(_fileName)) return;
            this.Enabled = false;
            ProcessAll();
            this.Enabled = true;
        }
    }
}
