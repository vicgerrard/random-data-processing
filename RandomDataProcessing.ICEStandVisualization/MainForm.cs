﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RandomDataProcessing.Core;
using RandomDataProcessing.Core.ICE;
using RandomDataProcessing.Extensions;
using ZedGraph;

namespace RandomDataProcessing.ICEStandVisualization
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private readonly Random _random = new Random();
        private void MainForm_Load(object sender, EventArgs e)
        {
            double[] values = new double[1000];
            for (int i = 0; i < values.Length; i++)
                values[i] = Math.Sin(i);

            var spectr = FourierProcessing.GetPhaseFourier(values);
            var xValues = ValuesHelper.GetSpectrXValues(spectr.Length, 1);

            for (int i = 0; i < values.Length; i++)
                chart1.Series.First().Points.AddXY(xValues[i], spectr[i]);

            chart1.Refresh();
            //DrawCurve(cylinderGraph, values.ToList());
            //var spectr = FourierProcessing.GetPhaseFourier(values);
            //spectrGraph.AddCurve(ValuesHelper.GetXValues(spectr.Length), spectr, false);



            //double[][] values = new double[4][];
            //double[][] spectres = new double[4][];
            //for (int i = 0; i < 4; i++)
            //{
            //    values[i] = Enumerable.Range(0, 1000).Select(x => MathHelper.Sin(x)).ToArray();
            //    //if (i == 1)
            //    //    values[i] = Enumerable.Range(0, 1000).Select(x => 0.5d * MathHelper.Sin(1.4d * x + i * 90)).ToArray();
            //    DrawCurve(cylinderGraph, values[i].ToList());
            //    spectres[i] = FourierProcessing.GetPhaseFourier(values[i]);
            //    spectrGraph.AddCurve(ValuesHelper.GetXValues(spectres[i].Length), spectres[i], false);
            //}
            ////textBox1.Text = RandomQualityChecker.Check(spectres[0]).ToString();
            ////textBox2.Text = RandomQualityChecker.Check(spectres[1]).ToString();
            ////textBox3.Text = RandomQualityChecker.Check(spectres[2]).ToString();
            ////textBox4.Text = RandomQualityChecker.Check(values[3]).ToString();
        }

        private void DrawCurve(ZedGraphControl graph, List<double> values)
        {
            var xValues = ValuesHelper.GetXValues(values.Count);
            graph.AddCurve(xValues, values.ToArray(), false);
        }
    }
}
