﻿namespace RandomDataProcessing.FiltersVisualization
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.rbBsf = new System.Windows.Forms.RadioButton();
            this.rbBpf = new System.Windows.Forms.RadioButton();
            this.rbHpf = new System.Windows.Forms.RadioButton();
            this.rbLpf = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.convSpectrGraph = new ZedGraph.ZedGraphControl();
            this.pgpSpectrGraph = new ZedGraph.ZedGraphControl();
            this.filterSpectrGraph = new ZedGraph.ZedGraphControl();
            this.convGraph = new ZedGraph.ZedGraphControl();
            this.pgpGraph = new ZedGraph.ZedGraphControl();
            this.filterGraph = new ZedGraph.ZedGraphControl();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.rbBsf);
            this.splitContainer1.Panel1.Controls.Add(this.rbBpf);
            this.splitContainer1.Panel1.Controls.Add(this.rbHpf);
            this.splitContainer1.Panel1.Controls.Add(this.rbLpf);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tableLayoutPanel1);
            this.splitContainer1.Size = new System.Drawing.Size(885, 444);
            this.splitContainer1.SplitterDistance = 30;
            this.splitContainer1.TabIndex = 0;
            // 
            // rbBsf
            // 
            this.rbBsf.AutoSize = true;
            this.rbBsf.Location = new System.Drawing.Point(287, 8);
            this.rbBsf.Name = "rbBsf";
            this.rbBsf.Size = new System.Drawing.Size(94, 17);
            this.rbBsf.TabIndex = 3;
            this.rbBsf.Text = "BandStopFilter";
            this.rbBsf.UseVisualStyleBackColor = true;
            this.rbBsf.CheckedChanged += new System.EventHandler(this.rbBsf_CheckedChanged);
            // 
            // rbBpf
            // 
            this.rbBpf.AutoSize = true;
            this.rbBpf.Location = new System.Drawing.Point(193, 8);
            this.rbBpf.Name = "rbBpf";
            this.rbBpf.Size = new System.Drawing.Size(95, 17);
            this.rbBpf.TabIndex = 2;
            this.rbBpf.Text = "BandPassFilter";
            this.rbBpf.UseVisualStyleBackColor = true;
            this.rbBpf.CheckedChanged += new System.EventHandler(this.rbBpf_CheckedChanged);
            // 
            // rbHpf
            // 
            this.rbHpf.AutoSize = true;
            this.rbHpf.Location = new System.Drawing.Point(102, 8);
            this.rbHpf.Name = "rbHpf";
            this.rbHpf.Size = new System.Drawing.Size(92, 17);
            this.rbHpf.TabIndex = 1;
            this.rbHpf.Text = "HighPassFilter";
            this.rbHpf.UseVisualStyleBackColor = true;
            this.rbHpf.CheckedChanged += new System.EventHandler(this.rbHpf_CheckedChanged);
            // 
            // rbLpf
            // 
            this.rbLpf.AutoSize = true;
            this.rbLpf.Checked = true;
            this.rbLpf.Location = new System.Drawing.Point(11, 8);
            this.rbLpf.Name = "rbLpf";
            this.rbLpf.Size = new System.Drawing.Size(90, 17);
            this.rbLpf.TabIndex = 0;
            this.rbLpf.TabStop = true;
            this.rbLpf.Text = "LowPassFilter";
            this.rbLpf.UseVisualStyleBackColor = true;
            this.rbLpf.CheckedChanged += new System.EventHandler(this.rbLpf_CheckedChanged);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.convSpectrGraph, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.pgpSpectrGraph, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.filterSpectrGraph, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.convGraph, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.pgpGraph, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.filterGraph, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(885, 410);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // convSpectrGraph
            // 
            this.convSpectrGraph.Dock = System.Windows.Forms.DockStyle.Fill;
            this.convSpectrGraph.Location = new System.Drawing.Point(593, 208);
            this.convSpectrGraph.Name = "convSpectrGraph";
            this.convSpectrGraph.ScrollGrace = 0D;
            this.convSpectrGraph.ScrollMaxX = 0D;
            this.convSpectrGraph.ScrollMaxY = 0D;
            this.convSpectrGraph.ScrollMaxY2 = 0D;
            this.convSpectrGraph.ScrollMinX = 0D;
            this.convSpectrGraph.ScrollMinY = 0D;
            this.convSpectrGraph.ScrollMinY2 = 0D;
            this.convSpectrGraph.Size = new System.Drawing.Size(289, 199);
            this.convSpectrGraph.TabIndex = 5;
            this.convSpectrGraph.UseExtendedPrintDialog = true;
            // 
            // pgpSpectrGraph
            // 
            this.pgpSpectrGraph.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgpSpectrGraph.Location = new System.Drawing.Point(298, 208);
            this.pgpSpectrGraph.Name = "pgpSpectrGraph";
            this.pgpSpectrGraph.ScrollGrace = 0D;
            this.pgpSpectrGraph.ScrollMaxX = 0D;
            this.pgpSpectrGraph.ScrollMaxY = 0D;
            this.pgpSpectrGraph.ScrollMaxY2 = 0D;
            this.pgpSpectrGraph.ScrollMinX = 0D;
            this.pgpSpectrGraph.ScrollMinY = 0D;
            this.pgpSpectrGraph.ScrollMinY2 = 0D;
            this.pgpSpectrGraph.Size = new System.Drawing.Size(289, 199);
            this.pgpSpectrGraph.TabIndex = 4;
            this.pgpSpectrGraph.UseExtendedPrintDialog = true;
            // 
            // filterSpectrGraph
            // 
            this.filterSpectrGraph.Dock = System.Windows.Forms.DockStyle.Fill;
            this.filterSpectrGraph.Location = new System.Drawing.Point(3, 208);
            this.filterSpectrGraph.Name = "filterSpectrGraph";
            this.filterSpectrGraph.ScrollGrace = 0D;
            this.filterSpectrGraph.ScrollMaxX = 0D;
            this.filterSpectrGraph.ScrollMaxY = 0D;
            this.filterSpectrGraph.ScrollMaxY2 = 0D;
            this.filterSpectrGraph.ScrollMinX = 0D;
            this.filterSpectrGraph.ScrollMinY = 0D;
            this.filterSpectrGraph.ScrollMinY2 = 0D;
            this.filterSpectrGraph.Size = new System.Drawing.Size(289, 199);
            this.filterSpectrGraph.TabIndex = 3;
            this.filterSpectrGraph.UseExtendedPrintDialog = true;
            // 
            // convGraph
            // 
            this.convGraph.Dock = System.Windows.Forms.DockStyle.Fill;
            this.convGraph.Location = new System.Drawing.Point(593, 3);
            this.convGraph.Name = "convGraph";
            this.convGraph.ScrollGrace = 0D;
            this.convGraph.ScrollMaxX = 0D;
            this.convGraph.ScrollMaxY = 0D;
            this.convGraph.ScrollMaxY2 = 0D;
            this.convGraph.ScrollMinX = 0D;
            this.convGraph.ScrollMinY = 0D;
            this.convGraph.ScrollMinY2 = 0D;
            this.convGraph.Size = new System.Drawing.Size(289, 199);
            this.convGraph.TabIndex = 2;
            this.convGraph.UseExtendedPrintDialog = true;
            // 
            // pgpGraph
            // 
            this.pgpGraph.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgpGraph.Location = new System.Drawing.Point(298, 3);
            this.pgpGraph.Name = "pgpGraph";
            this.pgpGraph.ScrollGrace = 0D;
            this.pgpGraph.ScrollMaxX = 0D;
            this.pgpGraph.ScrollMaxY = 0D;
            this.pgpGraph.ScrollMaxY2 = 0D;
            this.pgpGraph.ScrollMinX = 0D;
            this.pgpGraph.ScrollMinY = 0D;
            this.pgpGraph.ScrollMinY2 = 0D;
            this.pgpGraph.Size = new System.Drawing.Size(289, 199);
            this.pgpGraph.TabIndex = 1;
            this.pgpGraph.UseExtendedPrintDialog = true;
            // 
            // filterGraph
            // 
            this.filterGraph.Dock = System.Windows.Forms.DockStyle.Fill;
            this.filterGraph.Location = new System.Drawing.Point(3, 3);
            this.filterGraph.Name = "filterGraph";
            this.filterGraph.ScrollGrace = 0D;
            this.filterGraph.ScrollMaxX = 0D;
            this.filterGraph.ScrollMaxY = 0D;
            this.filterGraph.ScrollMaxY2 = 0D;
            this.filterGraph.ScrollMinX = 0D;
            this.filterGraph.ScrollMinY = 0D;
            this.filterGraph.ScrollMinY2 = 0D;
            this.filterGraph.Size = new System.Drawing.Size(289, 199);
            this.filterGraph.TabIndex = 0;
            this.filterGraph.UseExtendedPrintDialog = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(885, 444);
            this.Controls.Add(this.splitContainer1);
            this.Name = "MainForm";
            this.Text = "FilterVisualization";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.RadioButton rbBsf;
        private System.Windows.Forms.RadioButton rbBpf;
        private System.Windows.Forms.RadioButton rbHpf;
        private System.Windows.Forms.RadioButton rbLpf;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private ZedGraph.ZedGraphControl convSpectrGraph;
        private ZedGraph.ZedGraphControl pgpSpectrGraph;
        private ZedGraph.ZedGraphControl filterSpectrGraph;
        private ZedGraph.ZedGraphControl convGraph;
        private ZedGraph.ZedGraphControl pgpGraph;
        private ZedGraph.ZedGraphControl filterGraph;
    }
}

