﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RandomDataProcessing.Core;
using RandomDataProcessing.Extensions;

namespace RandomDataProcessing.FiltersVisualization
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            PaintLpf();
        }
        private static string _file1Path = "v1x11.dat";
        private static string _file2Path = "v1y11.dat";
        private const double Fcut = 130d;
        private const double Dt = 0.001d;
        private const int M = 64;
        private const double Fcut1 = 30d;
        private const double Fcut2 = 60d;
        private void PaintGraph(
            double[] filterXValues,
            double[] filterYValues)
        {
            filterGraph.AddCurve(filterXValues, filterYValues);
            var filterSpectrYValues = FourierProcessing.GetFourier(filterYValues);
            var filterSpectrXValues = ValuesHelper.GetXValues(filterSpectrYValues.Length);
            filterSpectrGraph.AddCurve(filterSpectrXValues, filterSpectrYValues);
            var file1 = FileProcessing.LoadDataFromFile(_file1Path);
            var file2 = FileProcessing.LoadDataFromFile(_file2Path);
            var pgpYValues = file1;
            var pgpXValues = ValuesHelper.GetXValues(pgpYValues.Length);
            pgpGraph.AddCurve(pgpXValues, pgpYValues);
            var pgpSpectrYValues = FourierProcessing.GetFourier(pgpYValues);
            var pgpSpectrXValues = ValuesHelper.GetXValues(pgpSpectrYValues.Length);
            pgpSpectrGraph.AddCurve(pgpSpectrXValues, pgpSpectrYValues);
            var convYValues = FilterProcessing.Convolution(pgpYValues,filterYValues);
            var convXValues = ValuesHelper.GetXValues(convYValues.Length);
            convGraph.AddCurve(convXValues, convYValues);
            var convSpectrYValues = FourierProcessing.GetFourier(convYValues);
            var convSpectrXValues = ValuesHelper.GetXValues(convSpectrYValues.Length);
            convSpectrGraph.AddCurve(convSpectrXValues, convSpectrYValues);
        }

        private void rbLpf_CheckedChanged(object sender, EventArgs e)
        {
            PaintLpf();
        }

        private void PaintLpf()
        {
            var yValues = FilterProcessing.LowPassFilter(Fcut, M, Dt);
            var xValues = ValuesHelper.GetSpectrXValues(yValues.Length, 1000);
            PaintGraph(xValues, yValues);
        }

        private void rbHpf_CheckedChanged(object sender, EventArgs e)
        {
            PaintHpf();
        }

        private void PaintHpf()
        {
            var yValues = FilterProcessing.HighPassFilter(Fcut, M, Dt);
            var xValues = ValuesHelper.GetSpectrXValues(yValues.Length, 1000);
            PaintGraph(xValues, yValues);
        }

        private void rbBpf_CheckedChanged(object sender, EventArgs e)
        {
            PaintBpf();
        }

        private void PaintBpf()
        {
            var yValues = FilterProcessing.BandPassFilter(Fcut1,Fcut2, M, Dt);
            var xValues = ValuesHelper.GetSpectrXValues(yValues.Length, 1000);
            PaintGraph(xValues, yValues);
        }

        private void rbBsf_CheckedChanged(object sender, EventArgs e)
        {
            PaintBsf();
        }

        private void PaintBsf()
        {
            var yValues = FilterProcessing.BandStopFilter(Fcut1, Fcut2, M, Dt);
            var xValues = ValuesHelper.GetSpectrXValues(yValues.Length, 1000);
            PaintGraph(xValues, yValues);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }
    }
}
